#!/bin/bash
# Value for TERRAFORM_KEY, PROJECT_ID, ENV are taken from GITLAB CI environment variables
# ENV can be either: PROD, STAGE, DEV

ECHO=`which echo`
$ECHO $TERRAFORM_KEY > gcloud-service-key.json
gcloud auth activate-service-account --key-file gcloud-service-key.json
gcloud config set project $PROJECT_ID
gcloud services enable secretmanager.googleapis.com
gcloud services enable serviceusage.googleapis.com
gcloud services enable cloudbuild.googleapis.com
gcloud services enable sourcerepo.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
# Set Permissions
gcloud projects add-iam-policy-binding ${PROJECT_ID} --member serviceAccount:$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')@cloudbuild.gserviceaccount.com --role roles/owner
# Submit Build
gcloud builds submit --config "cloudbuild.yaml" --substitutions=_ENV=$ENV --timeout=1200s
